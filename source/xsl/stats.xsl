<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output omit-xml-declaration="yes" />

<xsl:template match="/">

<h2>Statistiques</h2>

<h3>Constitution</h3>

<h4>Date de création</h4>

<p>Les chronologies se basent sur les dates de création renseignées dans les 
données source.</p>

<p>Les dates de création ne correspondent pas nécessairement à la date de 
création du canal de diffusion (flux RSS, chaîne Peertube, Youtube, etc). 
Elles correspondent au jour de publication du premier média complet et 
«&#160;représentatif&#160;».</p>

<p>Par exemple, il n’est pas rare que les émissions de vulgarisation musicales 
sur Youtube soient d’abord des chaînes de musiciens, et que les vidéos de 
vulgarisation arrivent plus tard. Or c’est la création de l’émission de 
vulgarisation qui importe.</p>

<h3>Compte</h3>

<p>À  ce jour, <xsl:value-of select="count(//emission)"/> émissions sont 
recensées. Parmi elles, <xsl:value-of select="count(//emission[@stopped])"/> 
sont arrêtées.</p>

<figure>
  <img src="img/topcategories_by_count.svg" alt="Répartition des émissions dans les principales catégories" />
</figure>

<h3>Langues</h3>

<table>
  <tr>
    <th>Langue</th>
    <th>Compte</th>
  </tr>
  <tr>
    <td>Français</td>
    <td><xsl:value-of select="count(//lang[.='fr'])"/></td>
  </tr>
  <tr>
    <td>Anglais</td>
    <td><xsl:value-of select="count(//lang[.='en'])"/></td>
  </tr>
  <tr>
    <td>Espéranto</td>
    <td><xsl:value-of select="count(//lang[.='eo'])"/></td>
  </tr>
</table>

<h3>Hébergement des émission</h3>

<p>Certaines émissions cumulent les hébergements de vidéos, d’autres 
utilisent un hébergeur exclusivement. Peertube étant un système fédéré,
il est considéré ici comme un tout.</p>

<table>
  <tr>
    <th>Plateforme</th>
    <th>Compte</th>
  </tr>
  <tr>
    <td>Peertube</td>
    <td><xsl:value-of select="count(//url[@type = 'pt'])"/></td>
  </tr>
  <tr>
    <td>Youtube</td>
    <td><xsl:value-of select="count(//url[@type = 'yt'])"/></td>
  </tr>
  <tr>
    <td>Dailymotion</td>
    <td><xsl:value-of select="count(//url[@type = 'dlm'])"/></td>
  </tr>
</table>

<h3>Arts</h3>

<figure>
  <img src="img/arts_by_count.svg" alt="Répartition des émissions dans la catégorie Arts" />
</figure>

<figure>
  <img src="img/arts_by_dates.svg" alt="Chronologie des émissions dans la catégorie Arts" />
</figure>

<figure>
  <img src="img/arts_by_dates_sorted.svg" alt="Chronologie des émissions dans la catégorie Arts, catégories différenciées" />
</figure>

<h3>Sciences humaines</h3>

<figure>
  <img src="img/humansciences_by_count.svg" alt="Répartition des émissions dans la catégorie Sciences humaines" />
</figure>

<figure>
  <img src="img/humansciences_by_dates.svg" alt="Chronologie des émissions dans la catégorie Sciences humaines" />
</figure>

<figure>
  <img src="img/humansciences_by_dates_sorted.svg" alt="Chronologie des émissions dans la catégorie Sciences humaines, catégories différenciées" />
</figure>

<h3>Sciences naturelles</h3>

<figure>
  <img src="img/inhumansciences_by_count.svg" alt="Répartition des émissions dans la catégorie Sciences naturelles" />
</figure>

<figure>
  <img src="img/inhumansciences_by_dates.svg" alt="Chronologie des émissions dans la catégorie Sciences naturelles" />
</figure>

<figure>
  <img src="img/inhumansciences_by_dates_sorted.svg" alt="Chronologie des émissions dans la catégorie Sciences naturelles, catégories différenciées" />
</figure>

<h4>Détail</h4>

<xsl:element name="ul">
<xsl:for-each select="/emissions/category">
  <xsl:sort select="@name" order="ascending" data-type="text" />
  <xsl:element name="li">
    <xsl:value-of select="@name" />
    <xsl:choose>
      <xsl:when test="count(./emission) = 0">&#160;(<xsl:value-of select="count(./*/emission)"/>)</xsl:when>
      <xsl:otherwise>&#160;(<xsl:value-of select="count(./emission)"/>)</xsl:otherwise>
    </xsl:choose>
    <xsl:if test="subCategory">
      <xsl:element name="ul">
        <xsl:for-each select="subCategory">
          <xsl:sort select="@name" order="ascending" data-type="text" />
          <xsl:element name="li">
            <xsl:value-of select="@name" /><xsl:if test="count(./emission)">&#160;(<xsl:value-of select="count(./emission)"/>)</xsl:if>
          </xsl:element>
        </xsl:for-each>
      </xsl:element>
    </xsl:if>
  </xsl:element>
</xsl:for-each>
</xsl:element>

<p>Dernière mise-à-jour des données&#160;: <xsl:value-of select="//update"/></p>
</xsl:template>

</xsl:stylesheet>
