<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output omit-xml-declaration="yes" />

<xsl:template match="/">
<p>Ce site a pour ambition de lister un maximum d’émissions culturelles, ou 
tout du moins «&#160;sérieuses&#160;» présentes sur Internet.</p>

<p>Il a été créé parce qu’une personne m’a soutenu qu’il n’y avait aucun 
programme intéressant en sciences, en arts sur YouTube&#160;— et tout le Web en 
général. Comme c’était manifestement faux et que je suivais déjà une centaine 
de chaînes dans ce domaine, j’en ai fait une liste, qui a grossi et s’est 
éditorialisée.</p>

<h2>Contenu</h2>

<p>À  ce jour, cette liste totalise 
<xsl:value-of select="count(//emission)"/> émissions. Pour plus de détails, 
voyez la page de statistiques.</p>

<h2>Suivre les émissions</h2>

<h3>RSS/Atom</h3>

<p>Vous pouvez suivre ces émissions via un agrégateur de flux 
<a href="https://fr.wikipedia.org/wiki/RSS">RSS</a>. Vous serez à coup sûr 
notifiés de l’existence de nouvelles vidéos.</p>

<dl>
  <dt><b>GNU/Linux, Windows, Mac</b></dt>
  <dd>Vous pouvez utiliser <a href="https://gpodder.github.io/">gPodder</a>. 
  J’ai par ailleurs écrit un <a href="https://etnadji.fr/blog/cloche-youtube.html">tutoriel 
  à ce sujet</a>.</dd>
  <dt><b>Android</b></dt>
  <dd>Vous pouvez utiliser <a href="https://antennapod.org/">AntennaPod</a>,
  qui est un logiciel libre, complet et sans publicités.</dd>
</dl>

<p>Des versions OPML, permettant d’ajouter parmi tout le catalogue, sont 
disponibles&#160;:</p>

<ul>
  <li><a href="dl/emissions.opml">OPML complet</a></li>
  <li><a href="dl/emissions_audio.opml">Seulement les émissions audio</a></li>
  <li><a href="dl/emissions_video.opml">Seulement les émissions vidéo</a></li>
</ul>

<p>Cette version exclut tous les liens ne pointant pas vers du RSS mais 
comporte des liens RSS créés automatiquement à partir des adresses de chaines 
Youtube. Elle exclut aussi les flux des chaînes Dailymotion, dans la mesure 
où ceux-ci ne semblent plus fonctionner.</p>

<h3>Web</h3>

<p>Je vous invite par ailleurs à consulter les liens présentés via des 
interfaces respectueuses de la vie privée, telles qu’Invidious, Piped, etc. 
Pour vous rediriger automatiquement vers celles-ci, il existe le module 
pour navigateur web LibRedirect 
(<a href="https://addons.mozilla.org/firefox/addon/libredirect/">Firefox</a>, 
<a href="https://libredirect.github.io/download.html">Chrome</a>, 
<a href="https://libredirect.github.io/download.html">Edge</a>).</p>

<p>Sur Android, si vous tenez à suivre une émission via Youtube, préférez 
<a href="https://newpipe.net/">NewPipe</a> à l’application par défaut&#160;: 
c’est une application libre, sans publicités, avec lecture en arrière-plan et 
téléchargement.</p>

<h2>Actualités</h2>

<dl>
  <dt>2024</dt>
  <dd>
    <dl>
      <dt>15&#160;décembre</dt>
      <dd><i>Données</i>&#160;: Création de la sous-catégorie «&#160;Ethnologie&#160;».</dd>
    </dl>
  </dd>
  <dt>2023</dt>
  <dd>
    <dl>
      <dt>5&#160;décembre</dt>
      <dd><i>Données</i>&#160;: Création de la sous-catégorie «&#160;Architecture&#160;».</dd>
    </dl>
  </dd>
  <dt>2022</dt>
  <dd>
    <dl>
      <dt>14&#160;septembre</dt>
      <dd><i>Données</i>&#160;: La sous-catégorie «&#160;Histoire&#160;» devient «&#160;Histoire / Archéologie&#160;».</dd>
      <dt>6&#160;juin</dt>
      <dd><i>Données</i>&#160;: Gestion des émissions arrêtées (@stopped).</dd>
    </dl>
  </dd>
  <dt>2021</dt>
  <dd>
    <dl>
      <dt>26&#160;juin</dt>
      <dd><i>Construction</i>&#160;: Passage à CSS Grid.</dd>
      <dt>28&#160;février</dt>
      <dd><i>Données</i>&#160;: Ajout des fichiers OPML filtrés&#160;: audio ou video</dd>
    </dl>
  </dd>
  <dt>2020</dt>
  <dd>
    <dl>
      <dt>29&#160;septembre</dt>
      <dd><i>Données</i>&#160;: Harmonisation des titres de sous-catégories «&#160;Divers&#160;»</dd>
      <dt>26&#160;septembre</dt>
      <dd><i>FAQ</i>&#160;: Choix éditoriaux et critères d’ajout.</dd>
      <dt>7&#160;septembre</dt>
      <dd><i>Statistiques</i>&#160;: Graphiques d’évolution des sous-catégories dans le temps.</dd>
      <dt>25&#160;août</dt>
      <dd><i>Données</i>&#160;: Suppression d’une sous-catégorie (cf commit).</dd>
      <dt>21&#160;août</dt>
      <dd>
	<i>Statistiques</i>&#160;: Graphiques Matplotlib.<br/>
	<i>Données</i>&#160;: Suppression de deux catégories (cf commit).
      </dd>
      <dt>18&#160;août</dt>
      <dd><i>Statistiques</i>&#160;: Page</dd>
      <dt>30&#160;juillet</dt>
      <dd><i>Construction</i>&#160;: Passage à Cifereldo.</dd>
    </dl>
  </dd>
</dl>

<p>Dernière mise à jour des données et des OPML&#160;: <xsl:value-of select="//update"/></p>
</xsl:template>

</xsl:stylesheet>
