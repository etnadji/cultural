<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output omit-xml-declaration="yes" />

<xsl:template match="/"><xsl:apply-templates /></xsl:template>

<xsl:template match="emission">
  <xsl:element name="div">
    <xsl:attribute name="class">card</xsl:attribute>
    <xsl:attribute name="lang"><xsl:value-of select="lang" /></xsl:attribute>
    <xsl:attribute name="id"><xsl:value-of select="@id" /></xsl:attribute>
    <!-- Titre ======================================================== -->
    <xsl:element name="h2">
      <xsl:attribute name="class">t</xsl:attribute>
      <xsl:value-of select="title" />
      <xsl:element name="a">
	<xsl:attribute name="href"><xsl:value-of select="@id" />.html</xsl:attribute>
	<xsl:attribute name="class">permalink</xsl:attribute>
	Permalien
      </xsl:element>
    </xsl:element>
    <xsl:if test="lang != 'fr'">
      <xsl:element name="p">
	<xsl:attribute name="class">lang</xsl:attribute>
	<xsl:choose>
	  <xsl:when test="lang = 'en'">Émission en anglais.</xsl:when>
	  <xsl:when test="lang = 'eo'">Émission en espéranto.</xsl:when>
	  <xsl:otherwise>Émission en langue étrangère (<xsl:value-of select="lang" />)</xsl:otherwise>
	</xsl:choose>
      </xsl:element>
    </xsl:if>
    <xsl:if test="desc">
      <xsl:element name="p">
	<xsl:if test="desc/@cited">
	  <xsl:attribute name="class">cited</xsl:attribute>
	</xsl:if>
	<xsl:apply-templates select="desc" />
	<xsl:if test="desc/@cited">
	  <xsl:if test="desc/@src">
	    <xsl:element name="br" />
	    <xsl:element name="a">
	      <xsl:attribute name="href"><xsl:value-of select="desc/@src" /></xsl:attribute> [source]
	    </xsl:element>
	  </xsl:if>
	</xsl:if>
      </xsl:element>
    </xsl:if>
    <xsl:if test="links">
      <xsl:element name="div">
	<xsl:attribute name="class">grid</xsl:attribute>
      <xsl:if test="boolean(links/url[@type = 'web']) or boolean(links/url[@type = 'rss'])">
	<xsl:element name="div">
	  <xsl:attribute name="class">web</xsl:attribute>
	  <h3>Web / RSS</h3>
	  <xsl:element name="ul">
	    <xsl:for-each select="links/url">
	      <xsl:sort select="@type" order="ascending" data-type="text" />
	      <xsl:choose>
		<!-- Regular web -->
		<xsl:when test="@type = 'web'">
		  <xsl:element name="li">
		    <xsl:variable name="linkTitle">
		      <xsl:choose>
			<xsl:when test="@text"><xsl:value-of select="@text" /></xsl:when>
			<xsl:otherwise><xsl:value-of select="." /></xsl:otherwise>
		      </xsl:choose>
		    </xsl:variable>
		    <xsl:element name="a">
		      <xsl:attribute name="href"><xsl:value-of select="." /></xsl:attribute>
		      <xsl:value-of select="$linkTitle" />
		    </xsl:element>
		    <xsl:if test="@rssUrl">&#160;<a class="rss" href="{.}{@rssUrl}">RSS</a></xsl:if>
		  </xsl:element>
		</xsl:when>
		<!-- RSS feed -->
		<xsl:when test="@type = 'rss'">
		  <xsl:choose>
		    <xsl:when test="@text">
		      <li><a href="{.}"><xsl:value-of select="@text" /></a></li>
		    </xsl:when>
		    <xsl:otherwise>
		      <li><a href="{.}">Flux RSS / Atom</a></li>
		    </xsl:otherwise>
		  </xsl:choose>
		</xsl:when>
	      </xsl:choose>
	    </xsl:for-each>
	  </xsl:element>
	</xsl:element>
      </xsl:if>

      <xsl:if test="boolean(links/url[@type = 'yt']) or boolean(links/url[@type = 'dlm']) or boolean(links/url[@type = 'live']) or boolean(links/url[@type = 'pt'])">
	<xsl:element name="div">
	  <xsl:attribute name="class">video</xsl:attribute>
	  <h3>Vidéo</h3>
	  <xsl:element name="ul">
	    <xsl:for-each select="links/url">
	      <xsl:sort select="@type" order="ascending" data-type="text" />
	      <xsl:choose>
		<!-- Youtube -->
		<xsl:when test="@type = 'yt'">
		  <xsl:element name="li">
		    <xsl:choose>
		      <xsl:when test="@subType = 'channel'"><a href="https://www.youtube.com/channel/{.}">Chaine Youtube</a></xsl:when>
		      <xsl:when test="@subType = 'user'"><a href="https://www.youtube.com/user/{.}">Chaine Youtube</a></xsl:when>
		      <xsl:otherwise><a href="{.}"><xsl:value-of select="." /></a></xsl:otherwise>
		    </xsl:choose>
		    <xsl:text> </xsl:text>
		    <xsl:choose>
		      <xsl:when test="@subType = 'user'"><a href="https://www.youtube.com/feeds/videos.xml?user={.}" class="rss">RSS</a></xsl:when>
		      <xsl:when test="@subType = 'channel'"><a href="https://www.youtube.com/feeds/videos.xml?channel_id={.}" class="rss">RSS</a></xsl:when>
		    </xsl:choose>
		  </xsl:element>
		</xsl:when>
		<!-- Dailymotion -->
		<xsl:when test="@type = 'dlm'">
		  <xsl:element name="li">
		    <a href="https://www.dailymotion.com/{.}">Chaine Dailymotion</a>
		    <xsl:text> </xsl:text>
		    <a class="rss" href="https://www.dailymotion.com/rss/user/{.}">RSS</a>
		  </xsl:element>
		</xsl:when>
		<!-- Peertube -->
		<xsl:when test="@type = 'pt'">
		  <xsl:element name="li">
		    <a href="https://{@instance}/accounts/{.}/videos">
		      <xsl:value-of select="." />@<xsl:value-of select="@instance" />
		    </a><xsl:text> </xsl:text><a href="https://{@instance}/feeds/videos.xml?accountId={@id}" class="rss">RSS</a>
		  </xsl:element>
		</xsl:when>
		<!-- Livestream -->
		<xsl:when test="@type = 'live'">
		  <xsl:choose>
		    <xsl:when test="@subType = 'twitch'">
		      <li>
			<a href="https://www.twitch.tv/{.}">Chaine Twitch</a>
		      </li>
		    </xsl:when>
		    <xsl:otherwise>
		      <li><a href="{.}"><xsl:value-of select="." /></a></li>
		    </xsl:otherwise>
		  </xsl:choose>
		</xsl:when>
	      </xsl:choose>
	    </xsl:for-each>
	  </xsl:element>
	</xsl:element>
      </xsl:if>

      <xsl:if test="boolean(links/url[@type = 'tw']) or boolean(links/url[@type = 'md'])">
	<xsl:element name="div">
	  <xsl:attribute name="class">social</xsl:attribute>
	  <h3>Soutien / Réseaux</h3>
	  <xsl:element name="ul">
	    <xsl:for-each select="links/url">
	      <xsl:sort select="@type" order="ascending" data-type="text" />
	      <xsl:choose>
		<!-- Twitter -->
		<xsl:when test="@type = 'tw'">
		  <xsl:element name="li">
		    <a href="https://twitter.com/{.}">@<xsl:value-of select="." /></a>
		  </xsl:element>
		</xsl:when>
		<!-- Mastodon -->
		<xsl:when test="@type = 'md'">
		  <xsl:element name="li">
		    <xsl:element name="a">
		      <xsl:attribute name="href">https://<xsl:value-of select="@instance" />/@<xsl:value-of select="." /></xsl:attribute>
		      <xsl:value-of select="." />@<xsl:value-of select="@instance" />
		    </xsl:element>
		    <xsl:text> </xsl:text><a class="rss" href="https://{@instance}/@{.}.rss">RSS</a>
		  </xsl:element>
		</xsl:when>
	      </xsl:choose>
	    </xsl:for-each>
	  </xsl:element>
	</xsl:element>
      </xsl:if>

      <xsl:if test="boolean(links/url[@type = 'money'])">
	<xsl:element name="div">
	  <xsl:attribute name="class">money</xsl:attribute>
	  <h3>Soutien / Finances</h3>
	  <xsl:element name="ul">
	    <xsl:for-each select="links/url">
	      <xsl:sort select="@type" order="ascending" data-type="text" />
	      <xsl:choose>
		<!-- Financial support -->
		<xsl:when test="@type = 'money'">
		  <xsl:variable name="linkUrl">
		    <xsl:choose>
		      <xsl:when test="@subType = 'tipee'">https://www.tipeee.com/<xsl:value-of select="." /></xsl:when>
		      <xsl:when test="@subType = 'utip'">https://utip.io/<xsl:value-of select="." /></xsl:when>
		      <xsl:when test="@subType = 'patreon'">https://www.patreon.com/<xsl:value-of select="." /></xsl:when>
		      <xsl:otherwise><xsl:value-of select="." /></xsl:otherwise>
		    </xsl:choose>
		  </xsl:variable>
		  <xsl:variable name="linkTitle">
		    <xsl:choose>
		      <xsl:when test="@subType = 'tipee'">tipeee.com/<xsl:value-of select="." /></xsl:when>
		      <xsl:when test="@subType = 'utip'">utip.io/<xsl:value-of select="." /></xsl:when>
		      <xsl:when test="@subType = 'patreon'">patreon.com/<xsl:value-of select="." /></xsl:when>
		      <xsl:otherwise><xsl:value-of select="." /></xsl:otherwise>
		    </xsl:choose>
		  </xsl:variable>
		  <xsl:element name="li">
		    <xsl:element name="a">
		      <xsl:attribute name="href"><xsl:value-of select="$linkUrl" /></xsl:attribute>
		      <xsl:value-of select="$linkTitle" />
		    </xsl:element>
		  </xsl:element>
		</xsl:when>
	      </xsl:choose>
	    </xsl:for-each>
	  </xsl:element>
	</xsl:element>
      </xsl:if>

      <xsl:element name="div">
	<xsl:element name="ul">
	<xsl:for-each select="links/url">
	  <xsl:sort select="@type" order="ascending" data-type="text" />
	  <xsl:choose>
	    <xsl:when test="@type = 'yt'"></xsl:when>
	    <xsl:when test="@type = 'dlm'"></xsl:when>
	    <xsl:when test="@type = 'pt'"></xsl:when>
	    <xsl:when test="@type = 'web'"></xsl:when>
	    <xsl:when test="@type = 'rss'"></xsl:when>
	    <xsl:when test="@type = 'tw'"></xsl:when>
	    <xsl:when test="@type = 'md'"></xsl:when>
	    <xsl:when test="@type = 'money'"></xsl:when>
	    <!-- Unknown type -->
	    <xsl:otherwise>
	      <xsl:element name="li">
		<xsl:element name="a">
		  <xsl:attribute name="href"><xsl:value-of select="." /></xsl:attribute>
		  <xsl:value-of select="." />
		</xsl:element>
	      </xsl:element>
	    </xsl:otherwise>
	  </xsl:choose>
	</xsl:for-each>
	</xsl:element>
      </xsl:element>
      </xsl:element>
    </xsl:if>
  </xsl:element>
</xsl:template>

</xsl:stylesheet>
