<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output omit-xml-declaration="yes" />
<xsl:template match="/">
<h2>Foire aux questions</h2>

<ol>
  <li><a href="#retrait">Je n’ai pas envie que vous listiez mon émission.</a></li>
  <li><a href="#proposer">J’ai une émission à proposer&#160;!</a></li>
  <li><a href="#editorial">Quels sont les choix éditoriaux&#160;?</a></li>
  <li><a href="#politique">Pourquoi les émissions sur X sont dans la catégorie «&#160;Politique&#160;»&#160;?</a></li>
  <li><a href="#alimentation">Comment les données sont-elles alimentées&#160;?</a></li>
  <li><a href="#techno">Comment est fait ce site&#160;?</a></li>
</ol>

<h3 id="retrait">Je n’ai pas envie que vous listiez mon émission.</h3>

<p>Contactez moi, et je supprime votre émission de la liste aussi vite que 
possible.</p>

<h3 id="proposer">J’ai une émission à proposer&#160;!</h3>

<p>Certains contenus ne sont pas jugés recevables pour ne pas faire de cette 
liste d’émissions un fourre-tout. Lisez la section suivante, «&#160;Quels sont 
les choix éditoriaux&#160;?&#160;» avant de proposer une émission.
Vous pouvez proposer des émissions via 
<a href="https://framagit.org/etnadji/internet-cultural-programs">le dépôt 
GIT</a>, que ce soit 
<a href="https://framagit.org/etnadji/internet-cultural-programs/-/issues">en 
créant un ticket</a> ou, si vous vous y connaissez, en faisant une demande de 
fusion (<em>pull request</em>).</p>

<h3 id="editorial">Quels sont les choix éditoriaux&#160;?</h3>

<h4>Critères d’ajout de base</h4>

<ul>
  <li>L’émission doit avoir au moins un épisode, hors bande-annonce.</li>
  <li>Les chaines de tutoriels ne sont pas admises.</li>
  <li>Les cours théoriques sont admis.</li>
  <li>Le rythme de publication n’a aucune importance.</li>
</ul>

<h4>Émission sur l’art</h4>

<p>Les émissions donnant <b>un avis plutôt qu’une critique</b> sur une œuvre 
ne sont pas admises. Comprendre&#160;: l’émission peut dire qu’une œuvre est 
un chef-d’œuvre ou une m…, mais doit présenter une argumentation, une analyse 
du genre dans laquelle elle se place, etc.</p>

<p>Les émissions incitant systématiquement à acheter un produit culturel (hors 
séquences de sponsorisation) ne sont pas admises.</p>

<p>Les émissions peuvent avoir des producteurs privés ou publics, mais ne 
doivent pas être des émissions faisant la promotion des produits dudit 
producteur. Par exemple&#160;: une chaîne Youtube d’une maison d’édition faisant 
des présentations de ses livres ne pourra pas être integrée, mais <em>La 
mécanique du livre</em>, produite par une maison d’édition, est intégrée parce 
qu’elle présente les métiers du livre de manière générale.</p>

<h4>Émissions scientifiques</h4>

<p>Les émissions sur les sciences peuvent faire des approximations, de la 
vulgarisation voire se tromper quant aux théories évoquées mais doivent le 
faire de bonne foi (voir «&#160;Critères de suppression&#160;»).</p>

<h4>Critères de suppression</h4>

<p>Pour les émissions scientifiques, la représentation sophistique, 
délibérement fausse de faits, de champs disciplinaires (par exemple, les 
sciences humaines) constitue une cause de suppression de la liste.</p>

<h4>Critères d’étiquettage comme «&#160;arrêté&#160;»</h4>

<p>Une émission est étiquettée comme «&#160;arrêtée&#160;» quand il n’y a pas 
eu de nouveau contenu depuis 2&#160;ans, avec une marge concernant les 
émissions ayant de toute évidence un temps de production assez long.</p>

<h3 id="politique">Pourquoi les émissions sur X sont dans la catégorie 
«&#160;Politique&#160;»&#160;?</h3>

<p>Parce que X, c’est de la politique.</p>

<h3 iv="alimentation">Comment les données sont-elles alimentées&#160;?</h3>

<p>À la main, depuis le 8 janvier 2016, à en croire les <i>commits</i>. 
Oui, pour atteindre <xsl:value-of select="count(//emission)"/> émissions, 
ça prend du temps.</p>

<p>L’ajout se fait au gré des découvertes, des recommandations. L’origine de 
certaines de ces recommandations est à présent perdue, mais vous pourrez lire 
ci-dessous les sources enregistrées depuis que j’ai mis un place un moyen de… 
eh bien les enregistrer.</p>

<h4>Sources</h4>

<xsl:element name="ul">
<xsl:attribute name="style">max-width: 70ch;</xsl:attribute>
<xsl:for-each select="emissions/sources/s">
  <xsl:element name="li">
    <xsl:element name="a">
      <xsl:attribute name="href"><xsl:value-of select="surl" /></xsl:attribute>
      Lien
    </xsl:element>
    <xsl:if test="sdesc">
      <br/><xsl:value-of select="sdesc" />
    </xsl:if>
    <xsl:if test="@licence">
      <br/><b>Licence</b>&#160;: <xsl:value-of select="@licence" />
    </xsl:if>
  </xsl:element>
</xsl:for-each>
</xsl:element>

<h3 id="techno">Comment est fait ce site&#160;?</h3>

<p>Il s’agit d’un site statique, généré avec un script python utilisant LXML, 
Jinja, Matplotlib et Cifereldo. Cifereldo est une librairie formalisant et 
automatisant en partie la production d’un ouvrage sur plusieurs médiums. La 
«&#160;table des matières&#160;» et les figures de la page de statistiques sont 
reconstruites à la volée.</p>

</xsl:template>
</xsl:stylesheet>
