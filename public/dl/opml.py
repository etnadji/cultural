#!/usr/bin/python3
# -*- coding:Utf-8 -*-

"""
Docstring!
"""

# Imports ===============================================================#

import os
import sys

import xml.etree.ElementTree as ET

# Variables globales ====================================================#

__author__ = "Etienne Nadji <etnadji@eml.cc>"

# Classes ===============================================================#


class Emission:

    def __init__(self):
        self.title = None
        self.rss_urls = []

    def opmlified(self):
        opml = []

        if self.rss_urls:

            if self.title is None:
                title = ""
            else:
                title = 'title="{0}" '.format(self.title)

            for url in self.rss_urls:
                url = 'xmlUrl="{0}"'.format(url)

                line = '  <outline text="" {0}type="rss" {1}/>\n'.format(
                    title, url
                )

                if line not in opml:
                    opml.append(line)

        return opml

    def parse(self, xml, medias):

        if "media" in xml.attrib.keys():

            if medias != "all":
                if xml.attrib["media"] != medias:
                    return False

        for main in xml:

            if main.tag == "title":
                self.title = main.text.strip()

            if main.tag == "links":

                for link in main:

                    # Extracts RSS URLs

                    rss_url = False
                    url_type, url_subtype = False, False

                    if "type" in link.attrib.keys():
                        url_type = link.attrib["type"]

                    if "subType" in link.attrib.keys():
                        url_subtype = link.attrib["subType"]

                    if url_type:

                        if url_type == "rss":
                            rss_url = link.text

                        if url_type == "web":

                            if "rssUrl" in link.attrib.keys():
                                rss_url = "{0}{1}".format(
                                    link.text, link.attrib["rssUrl"]
                                )

                        if url_type == "pt":
                            rss_url = "https://"
                            rss_url += link.attrib["instance"]
                            rss_url += "/feeds/videos.xml?accountId="
                            rss_url += link.attrib["id"]

                        if url_type == "yt":

                            if url_subtype in ["user", "channel"]:

                                ref = {
                                    "user": "videos.xml?user=",
                                    "channel": "videos.xml?channel_id="
                                }

                                rss_url = "https://www.youtube.com/feeds/"
                                rss_url += ref[url_subtype]
                                rss_url += link.text

                    if rss_url:

                        if rss_url not in self.rss_urls:
                            self.rss_urls.append(rss_url)

        return bool(self.rss_urls)

# Fonctions =============================================================#


def parse(xml_file, medias="all"):
    data = {
        "update": None, "emissions": []
    }

    xml_file = os.path.realpath(xml_file)

    xml_parsed = ET.parse(xml_file)
    xml_root = xml_parsed.getroot()

    for main in xml_root:

        if main.tag == "update":
            data["update"] = main.text

        if main.tag == "category":

            for sub in main:

                if sub.tag == "emission":
                    emi = Emission()
                    success = emi.parse(sub, medias)

                    if success:
                        data["emissions"].append(emi)

                if sub.tag == "subCategory":

                    for emission in sub:

                        if emission.tag == "emission":
                            emi = Emission()
                            success = emi.parse(emission, medias)

                            if success:
                                data["emissions"].append(emi)

    return data


def opml(data, opml_file):

    if data:

        with open(opml_file, "w") as opmlf:
            opmlf.write('<?xml version="1.0" encoding="utf-8"?>\n')
            opmlf.write('<opml version="2.0">\n')

            opmlf.write('<head>\n')
            opmlf.write('  <title>Recommandations d\'émissions</title>\n')

            if data["update"] is not None:
                opmlf.write(
                    '  <dateCreated>{0}</dateCreated>\n'.format(
                        data["update"]
                    )
                )

            opmlf.write('</head>\n')
            opmlf.write('<body>\n')

            for emission in data["emissions"]:

                urls = [url for url in emission.opmlified() if url]

                for url in urls:
                    opmlf.write(url)

            opmlf.write('</body>\n')
            opmlf.write('</opml>\n')

    return bool(data)

# Programme =============================================================#


if __name__ == "__main__":
    # XML =>

    if sys.argv[-1] in ["video", "audio"]:
        MEDIAS = sys.argv[-1]
    else:
        MEDIAS = "all"

    source = os.path.realpath("reco.xml")
    data = parse(source, MEDIAS)

    # => OPML

    if MEDIAS == "audio":
        output = os.path.realpath("audio_reco.opml")
    elif MEDIAS == "video":
        output = os.path.realpath("video_reco.opml")
    else:
        output = os.path.realpath("reco.opml")

    SUCCESS = opml(data, output)

    # => EXIT

    if SUCCESS:
        sys.exit(0)
    else:
        sys.exit(1)

# vim:set shiftwidth=4 softtabstop=4:
