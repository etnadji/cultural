#!/usr/bin/python3
# -*- coding:Utf-8 -*-

"""
Construction du site etnadji.frama.io/cultural.

Nécessite lxml, matplotlib, numpy et cifereldo.
"""

# Imports ===============================================================#

import os
import sys
import shutil
import datetime
import subprocess
import copy

# LXML

import lxml.etree

# Matplotlib

import matplotlib.pyplot as plt
import numpy as np

# Cifereldo

import cifereldo.base as cifereldo
import cifereldo.xslt as cixslt
import cifereldo.jinja as cinja

# Variables globales ====================================================#

__author__ = "Etienne Nadji <etnadji@eml.cc>"

# Chain =================================================================#


class Cultural(cifereldo.Chain):
    """
    Chaine éditoriale.
    """

    def __init__(self, sugg_dir):
        cifereldo.Chain.__init__(self, "Cultural")

        self.export_dir = "public"
        self.sources_dir = "source"
        self.sugg_dir = os.path.realpath(sugg_dir)

    def prepare(self):
        cifereldo.Chain.prepare(self)

        try:
            os.mkdir(self.export_dir)
        except FileExistsError:
            pass


# Mediums ===============================================================#


class GitlabPages(cixslt.XSLMedium, cinja.JinjaMedium):
    """
    Medium de pages Gitlab.
    """

    def __init__(self):
        cixslt.XSLMedium.__init__(self, "gitlab")
        cinja.JinjaMedium.__init__(
            self, "gitlab", os.sep.join(["source", "templates"])
        )

        self.connect_eu(RecoCategory, self.categories)
        self.connect_eu(Show, self.show)

        for ueclass in [IndexPage, FaqPage, StatsPage]:
            self.connect_eu(ueclass, self.simple)

        for ueclass in [
            MainCategoriesCount,
            HumanSciences,
            InhumanSciences,
            Arts,
        ]:
            self.connect_eu(ueclass, self.barh)

        for ueclass in [HumanSciencesDates, InhumanSciencesDates, ArtsDates]:
            self.connect_eu(ueclass, self.lplot)

        for ueclass in [
            HumanSciencesMultipleDates,
            InhumanSciencesMultipleDates,
            ArtsMultipleDates,
        ]:
            self.connect_eu(ueclass, self.mlplot)

    def prepare(self, suggestions):
        print("Prepare GitLab pages")

        self.suggestions = suggestions

        # Public dirs ----------------------------------------------------
        self.web_dir = self.chain.export_dir
        self.dl_dir = os.sep.join([self.web_dir, "dl"])
        self.img_dir = os.sep.join([self.web_dir, "img"])

        # Source dirs ----------------------------------------------------
        self.xsl_dir = os.sep.join([self.chain.sources_dir, "xsl"])
        self.css_dir = os.sep.join([self.chain.sources_dir, "css"])

        # Making the public dirs -----------------------------------------
        for directory in [self.web_dir, self.dl_dir, self.img_dir]:
            try:
                os.mkdir(directory)
            except FileExistsError:
                pass

        # CSS ------------------------------------------------------------

        for style_file in ["style.css"]:
            in_path = os.sep.join([self.css_dir, style_file])
            out_path = os.sep.join([self.web_dir, style_file])

            shutil.copy(in_path, out_path)
            self._minify(out_path)

        # Downloads ------------------------------------------------------

        for dlfile in [
            ["reco.xml", "emissions.xml"],
            ["reco.opml", "emissions.opml"],
            ["audio_reco.opml", "emissions_audio.opml"],
            ["video_reco.opml", "emissions_video.opml"],
            ["opml.py", "opml.py"],
        ]:
            orig = os.sep.join([self.chain.sugg_dir, dlfile[0]])
            dest = os.sep.join([self.dl_dir, dlfile[1]])

            shutil.copy(orig, dest)

        # Generate the main Jinja template, with accurate table of contents
        self.prepare_nav(suggestions.xml)

    def prepare_nav(self, suggestions):
        def link_line(url, title, sub=False):
            """
            Return a HTML link to URL with title with the correct CSS class.
            """

            # Select the correct CSS class : category or subcategory
            line_class = {True: "sc", False: "c"}[sub]

            # return "<a class='{}' href='{}'>{}</a>\n".format(
            #     line_class, url, title
            # )
            return f"<a class='{line_class}' href='{url}'>{title}</a>\n"

        def nav_line(xml, sub=False):
            url = "{}.html".format(xml.attrib["anchor"])
            return link_line(url, xml.attrib["name"], sub)

        # Generate <nav> content from XML shows datas --------------------

        nav_text = ""

        nav_text += link_line("site-faq.html", "[FAQ]")
        nav_text += link_line("site-stats.html", "[Statistiques]")

        for category in suggestions:
            # Avoid comments
            if not isinstance(category, lxml.etree._Comment):
                if category.tag == "category":
                    # TODO FIXME Cette ligne est inutile grace à nav_line ???
                    # url = "{}.html".format(category.attrib["anchor"])

                    nav_text += nav_line(category)

                    for subcategory in category:
                        if subcategory.tag == "subCategory":
                            nav_text += nav_line(subcategory, True)

        # Generate the nav template to be used in other Jinja templates --

        page_template = '{% extends "basic.html" %}' + "\n"
        page_template += "{% block nav %}" + "\n"
        page_template += nav_text
        page_template += "{% endblock %}"

        tp_path = os.sep.join([self.templates_dir, "page.html"])

        with open(tp_path, "w", encoding="utf8") as tpf:
            tpf.write(page_template)

    def _pre_render(self, editorial_unit, context):
        if isinstance(editorial_unit, IndexPage):
            template = self.get_template("index.html")
        else:
            template = self.get_template("page.html")

        page_path = "{}.html".format(
            os.sep.join([self.web_dir, editorial_unit.slug])
        )

        if "verbose" in context:
            print("Rendering to", page_path)

        return template, page_path

    def categories(self, editorial_unit, context={}):
        template, page_path = self._pre_render(editorial_unit, context)

        xslt = self.make_xsl_proc(
            os.sep.join([self.xsl_dir, editorial_unit.datas["gitlab-xsl"]])
        )

        ue_content = str(xslt(editorial_unit.datas["xml"]))

        html = template.render(
            slug=editorial_unit.slug,
            name=editorial_unit.name,
            content=ue_content,
            pageclass="shows",
        )

        with open(page_path, "w", encoding="utf8") as uef:
            uef.write(html)

        self._minify(page_path)

    def show(self, editorial_unit, context={}):
        template, page_path = self._pre_render(editorial_unit, context)

        xslt = self.make_xsl_proc(
            os.sep.join([self.xsl_dir, editorial_unit.datas["gitlab-xsl"]])
        )

        ue_content = str(xslt(editorial_unit.datas["xml"]))

        current_category = self.suggestions.categories[editorial_unit.category]
        main_categ, sub_categ = False, False

        main_categ = {
            "anchor": editorial_unit.category,
            "title": current_category["title"],
        }

        if editorial_unit.sub_category:
            sub_categ = {
                "anchor": editorial_unit.sub_category,
                "title": current_category["subs"][editorial_unit.sub_category][
                    "title"
                ],
            }

        header = "<h2>"
        header += f"<a href='{main_categ['anchor']}.html'>"
        header += f"{main_categ['title']}</a>"

        if sub_categ:
            header += " &gt; "
            header += f"<a href='{sub_categ['anchor']}.html'>"
            header += f"{sub_categ['title']}</a>"

        header += "</h2>"
        ue_content = header + ue_content

        html = template.render(
            slug=editorial_unit.slug,
            name=editorial_unit.name,
            content=ue_content,
            pageclass="show",
        )

        with open(page_path, "w", encoding="utf8") as uef:
            uef.write(html)

        self._minify(page_path)

    def simple(self, editorial_unit, context={}):
        template, page_path = self._pre_render(editorial_unit, context)

        xslt = self.make_xsl_proc(
            os.sep.join([self.xsl_dir, editorial_unit.datas["gitlab-xsl"]])
        )

        ue_content = str(xslt(editorial_unit.datas["xml"]))

        html = template.render(
            slug=editorial_unit.slug,
            name=editorial_unit.name,
            content=ue_content,
            pageclass="text",
        )

        with open(page_path, "w", encoding="utf8") as uef:
            uef.write(html)

        self._minify(page_path)

    def barh(self, editorial_unit, context={}):
        filepath = editorial_unit.barh(self.img_dir, "svg")
        self._minify(filepath)

    def lplot(self, editorial_unit, context={}):
        filepath = editorial_unit.lplot(self.img_dir, "svg")
        self._minify(filepath)

    def mlplot(self, editorial_unit, context={}):
        filepath = editorial_unit.multiplelplot(self.img_dir, "svg")
        self._minify(filepath)

    def _minify(self, filepath):
        subprocess.call("minify {0} > {0}.min".format(filepath), shell=True)
        os.remove(filepath)
        os.rename(filepath + ".min", filepath)


# Editorial units =======================================================#


class RecoCategory(cifereldo.EditorialUnit):
    """
    Show recommandations category page.

    :type slug: string
    :param slug: slug of the page
    :type category: string
    :param category: Category id
    :type sub_category: boolean
    :param sub_category: Is this a sub category ?
    """

    def __init__(self, slug, category, sub_category=False):
        cifereldo.EditorialUnit.__init__(self)

        self.slug = slug
        self.category = category
        self.sub_category = sub_category

    def load_data(self, data):
        cifereldo.EditorialUnit.load_data(self, data)

        self.datas = data
        self.name = data["xml"].attrib["name"]


class Show(cifereldo.EditorialUnit):
    def __init__(self, slug, category, sub_category=False):
        cifereldo.EditorialUnit.__init__(self)

        self.slug = slug
        self.category = category
        self.sub_category = sub_category

    def load_data(self, data):
        cifereldo.EditorialUnit.load_data(self, data)

        self.datas = data

        # Load the page title from show data
        for element in data["xml"]:
            if element.tag == "title":
                self.name = element.text.strip()


class SimpleXSLPage(cifereldo.EditorialUnit):
    """
    Basic XML/XSL page.

    :type slug: string
    :param slug: slug of the page
    :type data: dict
    :param data: EditorialUnit datas to load
    """

    def __init__(self, slug, data=False):
        cifereldo.EditorialUnit.__init__(self)

        self.slug = slug

        if data:
            self.load_data(data)

    def load_data(self, data):
        cifereldo.EditorialUnit.load_data(self, data)

        self.datas = data
        self.name = False

        if "xml" in self.datas:
            self.datas["xml"] = self.datas["xml"].xml


class IndexPage(SimpleXSLPage):
    """
    Index page.

    :type data: dict
    :param data: EditorialUnit datas to load
    """

    def __init__(self, data=False):
        SimpleXSLPage.__init__(self, "index", data)


class FaqPage(SimpleXSLPage):
    """
    FAQ page.

    :type data: dict
    :param data: EditorialUnit datas to load
    """

    def __init__(self, data=False):
        SimpleXSLPage.__init__(self, "site-faq", data)
        self.name = "Foire aux questions"


class StatsPage(SimpleXSLPage):
    """
    Statistics page.

    :type data: dict
    :param data: EditorialUnit datas to load
    """

    def __init__(self, data=False):
        SimpleXSLPage.__init__(self, "site-stats", data)
        self.name = "Statistiques"


class MPLFigure(cifereldo.EditorialUnit):
    """
    Matplotlib figure

    :type slug: string
    :param slug: slug of the image
    :type data: dict
    :param data: EditorialUnit datas to load
    """

    def __init__(self, slug, data=False):
        self.slug = slug
        self.plot = {}

        if data:
            self.load_data(data)

    def load_data(self, data):
        cifereldo.EditorialUnit.load_data(self, data)

    def barh(self, folder, exp_format="svg"):
        """
        Make a bar chart.
        """

        plt.rcdefaults()

        y_pos = np.arange(len(self.plot["labels"]))

        plt.barh(y_pos, self.plot["values"], align="center", alpha=None)
        plt.yticks(y_pos, self.plot["labels"])

        try:
            plt.title(self.plot["title"])
        except KeyError:
            pass

        try:
            plt.xlabel(self.plot["xlabel"])
        except KeyError:
            pass

        plt.tight_layout()

        if folder:
            if not folder.endswith(os.sep):
                folder += os.sep

        filepath = "{}{}.{}".format(folder, self.slug, exp_format)

        print("Rendering bar chart to", filepath)

        plt.savefig(filepath, format=exp_format, transparent=True)
        plt.close()

        return filepath

    def pie(self, folder, exp_format="svg"):
        """
        Make a pie chart.
        """

        plt.rcdefaults()

        colors = ["gold", "yellowgreen", "lightcoral", "lightskyblue"]

        # Plot
        plt.pie(
            self.plot["values"],
            labels=self.plot["labels"],
            colors=colors,
            autopct="%1.1f%%",
            shadow=True,
            startangle=140,
        )

        plt.axis("equal")

        plt.tight_layout()

        if folder:
            if not folder.endswith(os.sep):
                folder += os.sep

        filepath = "{}{}.{}".format(folder, self.slug, exp_format)

        print("Rendering pie chart to", filepath)

        plt.savefig(filepath, format=exp_format, transparent=True)
        plt.close()

        return filepath

    def lplot(self, folder, exp_format="svg"):
        plt.rcdefaults()

        plt.plot(self.plot["labels"], self.plot["values"])

        try:
            plt.xlabel(self.plot["xlabel"])
        except KeyError:
            pass

        plt.title(self.plot["title"])

        plt.gcf().autofmt_xdate()

        if folder:
            if not folder.endswith(os.sep):
                folder += os.sep

        filepath = "{}{}.{}".format(folder, self.slug, exp_format)

        print("Rendering line chart to", filepath)

        plt.savefig(filepath, format=exp_format, transparent=True)
        plt.close()

        return filepath

    def multiplelplot(self, folder, exp_format="svg"):
        plt.rcdefaults()

        fig, ax = plt.subplots(1, 1, figsize=(10, 7))

        plots = []
        plot_labels = []

        for plot_line in self.plot["values"]:
            if isinstance(plot_line, dict):
                plots = plots + ax.plot(
                    self.plot["labels"],
                    plot_line["values"],
                    label=plot_line["legend"],
                )
                plot_labels.append(plot_line["legend"])

        fig.legend(
            plots,
            labels=plot_labels,
            loc="center left",
            borderaxespad=0.1,
            title="Catégories",
            frameon=False,
        )

        plt.subplots_adjust(left=self.plot["legend-adjust"])
        # plt.subplots_adjust(right=self.plot["legend-adjust"])

        try:
            plt.xlabel(self.plot["xlabel"])
        except KeyError:
            pass

        plt.title(self.plot["title"])

        plt.gcf().autofmt_xdate()

        if folder:
            if not folder.endswith(os.sep):
                folder += os.sep

        filepath = "{}{}.{}".format(folder, self.slug, exp_format)

        print("Rendering multiple lines chart to", filepath)

        plt.savefig(filepath, format=exp_format, transparent=True)
        plt.close()

        return filepath


class CategDateMultiplePlot(MPLFigure):
    def __init__(self, slug, anchor, data=False):
        MPLFigure.__init__(self, slug, data)
        self.anchor = anchor

    def load_data(self, data):
        MPLFigure.load_data(self, data)

        def timestamp(d):
            return datetime.datetime.fromtimestamp(int(d))

        global_dates = []

        category = [
            c
            for c in data["xml"].findall("category")
            if c.attrib["anchor"] == self.anchor
        ][0]

        values = []

        for c in category:

            if c.tag == "subCategory":

                do = True

                for subcateg in values:
                    if subcateg["anchor"] == c.attrib["anchor"]:
                        do = False
                        break

                if do:
                    current_value = {
                        "anchor": c.attrib["anchor"],
                        "legend": c.attrib["name"],
                        "values": [],
                    }

                    for cc in c:
                        d = cc.get("creation")

                        if d is not None and d:
                            ts = timestamp(d)
                            global_dates.append(ts)
                            current_value["values"].append(ts)

                    values.append(current_value)

        for sc in values:

            tmp_values = copy.deepcopy(global_dates)

            for index, date in enumerate(global_dates):

                if date in sc["values"]:
                    pass
                else:
                    tmp_values[index] = 0

            tmp_count = 0

            for index, value in enumerate(tmp_values):
                if isinstance(value, datetime.datetime):
                    tmp_count += 1
                    tmp_values[index] = tmp_count
                else:
                    if index != 0:
                        if tmp_values[index - 1] != 0:
                            tmp_values[index] = tmp_values[index - 1]

            sc["values"] = tmp_values

        # count = [i for i in range(len(dates))]

        # Sorted dates as X
        labels = sorted(global_dates)

        self.plot["values"] = values
        self.plot["labels"] = labels


class CategDatePlot(MPLFigure):
    def __init__(self, slug, anchor, data=False):
        MPLFigure.__init__(self, slug, data)
        self.anchor = anchor

    def load_data(self, data):
        MPLFigure.load_data(self, data)

        def timestamp(d):
            return datetime.datetime.fromtimestamp(int(d))

        dates = []

        category = [
            c
            for c in data["xml"].findall("category")
            if c.attrib["anchor"] == self.anchor
        ][0]

        for c in category:

            # Add emission date

            if c.tag == "subCategory":

                for cc in c:
                    d = cc.get("creation")

                    if d is not None and d:
                        dates.append(timestamp(d))
            else:
                d = c.get("creation")

                if d is not None and d:
                    dates.append(timestamp(d))

        # We count the dates in order to make plot values as X
        count = [i for i in range(len(dates))]

        # Sorted dates as X
        dates = sorted(dates)

        self.plot["values"] = count
        self.plot["labels"] = dates


class HumanSciencesMultipleDates(CategDateMultiplePlot):
    def __init__(self, data=False):
        CategDateMultiplePlot.__init__(
            self, "humansciences_by_dates_sorted", "schum", data
        )

    def load_data(self, data):
        CategDateMultiplePlot.load_data(self, data)

        self.plot["title"] = "Sciences humaines : chronologie différenciée"
        # Right legend
        # self.plot["legend-adjust"] = 0.82
        # Left legend
        self.plot["legend-adjust"] = 0.2


class HumanSciencesDates(CategDatePlot):
    def __init__(self, data=False):
        CategDatePlot.__init__(self, "humansciences_by_dates", "schum", data)

    def load_data(self, data):
        CategDatePlot.load_data(self, data)

        self.plot["title"] = "Sciences humaines : chronologie globale"


class InhumanSciencesDates(CategDatePlot):
    def __init__(self, data=False):
        CategDatePlot.__init__(self, "inhumansciences_by_dates", "scnat", data)

    def load_data(self, data):
        CategDatePlot.load_data(self, data)

        total = len(self.plot["values"])
        self.plot["title"] = "Sciences naturelles : chronologie globale"


class InhumanSciencesMultipleDates(CategDateMultiplePlot):
    def __init__(self, data=False):
        CategDateMultiplePlot.__init__(
            self, "inhumansciences_by_dates_sorted", "scnat", data
        )

    def load_data(self, data):
        CategDateMultiplePlot.load_data(self, data)

        self.plot["title"] = "Sciences naturelles : chronologie différenciée"
        # Right legend
        # self.plot["legend-adjust"] = 0.78
        # Left legend
        self.plot["legend-adjust"] = 0.25


class ArtsDates(CategDatePlot):
    def __init__(self, data=False):
        CategDatePlot.__init__(self, "arts_by_dates", "arts", data)

    def load_data(self, data):
        CategDatePlot.load_data(self, data)

        total = len(self.plot["values"])
        self.plot["title"] = "Arts : chronologie globale"


class ArtsMultipleDates(CategDateMultiplePlot):
    def __init__(self, data=False):
        CategDateMultiplePlot.__init__(
            self, "arts_by_dates_sorted", "arts", data
        )

    def load_data(self, data):
        CategDateMultiplePlot.load_data(self, data)

        self.plot["title"] = "Arts : chronologie différenciée"
        # Right legend
        # self.plot["legend-adjust"] = 0.78
        # Left legend
        self.plot["legend-adjust"] = 0.25


class CategCountPlot(MPLFigure):
    def __init__(self, slug, anchor, data=False):
        MPLFigure.__init__(self, slug, data)
        self.anchor = anchor

    def load_data(self, data, splitlabel=False):
        def countSort(i):
            return i["count"]

        MPLFigure.load_data(self, data)

        category = [
            c
            for c in data["xml"].findall("category")
            if c.attrib["anchor"] == self.anchor
        ][0]

        subc_count = []

        for c in category:

            if c.tag == "subCategory":
                categ = {"name": c.attrib["name"], "count": 0}

                shows = c.findall("emission")
                categ["count"] = len(shows)

                subc_count.append(categ)

        # subc_sorted = sorted(categs_count, key=countSort, reverse=True)
        subc_sorted = sorted(subc_count, key=countSort)

        if splitlabel:
            self.plot["labels"] = [
                "\n".join([ii.strip() for ii in i["name"].split(splitlabel)])
                for i in subc_sorted
            ]
        else:
            self.plot["labels"] = [i["name"] for i in subc_sorted]

        self.plot["values"] = [i["count"] for i in subc_sorted]


class HumanSciences(CategCountPlot):
    def __init__(self, data=False):
        CategCountPlot.__init__(self, "humansciences_by_count", "schum", data)

    def load_data(self, data):
        CategCountPlot.load_data(self, data)

        total = sum(self.plot["values"])
        self.plot["title"] = "Sciences humaines : répartition"
        self.plot["xlabel"] = "Compte d’émissions (total = {})".format(total)


class InhumanSciences(CategCountPlot):
    def __init__(self, data=False):
        CategCountPlot.__init__(
            self, "inhumansciences_by_count", "scnat", data
        )

    def load_data(self, data):
        CategCountPlot.load_data(self, data)

        total = sum(self.plot["values"])
        self.plot["title"] = "Sciences naturelles : répartition"
        self.plot["xlabel"] = "Compte d’émissions (total = {})".format(total)


class Arts(CategCountPlot):
    def __init__(self, data=False):
        CategCountPlot.__init__(self, "arts_by_count", "arts", data)

    def load_data(self, data):
        CategCountPlot.load_data(self, data)

        total = sum(self.plot["values"])
        self.plot["title"] = "Arts : répartition"
        self.plot["xlabel"] = "Compte d’émissions (total = {})".format(total)


class MainCategoriesCount(MPLFigure):
    def __init__(self, data=False):
        MPLFigure.__init__(self, "topcategories_by_count")

    def load_data(self, data):
        def countSort(i):
            return i["count"]

        MPLFigure.load_data(self, data)

        categories = data["xml"].findall("category")

        categs_count = []

        for c in categories:
            categ = {"name": c.attrib["name"], "count": 0}
            shows = c.findall("emission")

            if len(shows):
                categ["count"] = len(shows)
            else:
                shows = c.findall("subCategory/emission")
                categ["count"] = len(shows)

            categs_count.append(categ)

        # categs_sorted = sorted(categs_count, key=countSort, reverse=True)
        categs_sorted = sorted(categs_count, key=countSort)

        self.plot["labels"] = [
            "\n".join(i["name"].split()) for i in categs_sorted
        ]

        self.plot["values"] = [i["count"] for i in categs_sorted]
        total = sum(self.plot["values"])
        self.plot["title"] = "Catégories principales : répartition"
        self.plot["xlabel"] = "Compte d’émissions (total = {})".format(total)


# Stores ================================================================#


class Suggestions(cifereldo.Store):
    def __init__(self, source_filepath):
        cifereldo.Store.__init__(self)

        self.filepath = os.path.realpath(source_filepath)
        self.xml = None

        self.categories = {}

        if os.path.exists(self.filepath):
            self.load()

    def load(self):
        if not self.loaded:
            self.xml = lxml.etree.parse(self.filepath).getroot()
            cifereldo.Store.load(self)

    def get_ues(self):
        """
        Load every category and subcategory as editorial units and append them
        to the editorial units global list.
        """

        def make_show(xml, category, sub_category=False):
            show_eu = Show(xml.attrib["id"], current_category, sub_category)
            show_eu.load_data({"xml": xml, "gitlab-xsl": "show.xsl"})
            return show_eu

        def make_categ(xml, category, sub_category=False):

            if sub_category:
                sub_category = xml.attrib["anchor"]
                gitlab_xsl = "subcategory.xsl"
            else:
                gitlab_xsl = "category.xsl"

            category_eu = RecoCategory(
                xml.attrib["anchor"], category, sub_category
            )

            category_eu.load_data({"xml": xml, "gitlab-xsl": gitlab_xsl})

            return category_eu

        ues = []

        for category in self.xml:

            if not isinstance(category, lxml.etree._Comment):

                if category.tag == "category":
                    # Category anchor = category slug
                    current_category = category.attrib["anchor"]

                    self.categories[current_category] = {
                        "title": category.attrib["name"],
                        "subs": {},
                    }

                    # Load the category as a EU
                    category_eu = make_categ(category, current_category)
                    ues.append(category_eu)

                    for sub in category:

                        # If this category has shows, load every show as
                        # an independant EU

                        if sub.tag == "emission":
                            show_eu = make_show(sub, current_category)
                            ues.append(show_eu)

                        # If this category has subcategories, each of
                        # them are also EUs

                        if sub.tag == "subCategory":
                            subcategory_eu = make_categ(
                                sub, current_category, True
                            )
                            ues.append(subcategory_eu)

                            self.categories[current_category]["subs"][
                                sub.attrib["anchor"]
                            ] = {"title": sub.attrib["name"]}

                            # Load subcategory shows as independant EUs

                            for ter in sub:
                                if ter.tag == "emission":
                                    show_eu = make_show(
                                        ter,
                                        current_category,
                                        sub.attrib["anchor"],
                                    )
                                    ues.append(show_eu)

        return ues


# Fonctions =============================================================#


def main():
    # Publishing chain ------------------------------------

    # Cultural takes raw datas folder as parameter
    cec = Cultural("internet-cultural-programs")

    # Mediums ---------------------------------------------

    gitlab = GitlabPages()

    cec.connect_medium(gitlab)

    # Editorial units -------------------------------------

    ues = []

    # Parse cultural programs XML datas once for all mediums
    suggestions = Suggestions(
        os.sep.join(["internet-cultural-programs/", "reco.xml"])
    )

    # Really static pages

    statics = [
        [FaqPage, "faq.xsl"],
        [IndexPage, "index.xsl"],
        [StatsPage, "stats.xsl"],
    ]

    for ue_class, xsl_file in statics:
        ue_instance = ue_class({"xml": suggestions, "gitlab-xsl": xsl_file})
        ues.append(ue_instance)

    # Load every category and subcategory as editorial units
    # ues = suggestions_to_ues(ues, suggestions)
    ues += suggestions.get_ues()

    # Matplotlib figures

    for ue_class in [
        MainCategoriesCount,
        HumanSciences,
        InhumanSciences,
        Arts,
        HumanSciencesDates,
        InhumanSciencesDates,
        ArtsDates,
        HumanSciencesMultipleDates,
        InhumanSciencesMultipleDates,
        ArtsMultipleDates,
    ]:
        ue_instance = ue_class()
        ue_instance.load_data({"xml": suggestions.xml})
        ues.append(ue_instance)

    # Preparing chain and mediums -------------------------

    cec.prepare()
    gitlab.prepare(suggestions)

    # Rendering all mediums -------------------------------

    cec.render(gitlab, ues, {"verbose": True})

    # The end ---------------------------------------------

    gitlab.finish()

    return 0


# Programme =============================================================#

if __name__ == "__main__":
    sys.exit(main())

# vim:set shiftwidth=4 softtabstop=4:
