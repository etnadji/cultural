.PHONY: all css content nav


all: css data
	python3 cultural.py

data: 
	git submodule update --recursive --remote

css: 
	scss source/css/style.scss > source/css/style.css
